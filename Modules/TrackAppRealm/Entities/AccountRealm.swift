//
//  AccountRealm.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 27/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation
import RealmSwift
import TrackAppDomain

@objcMembers
class AccountRealm: Object {
    dynamic var id: Int = 0
    dynamic var name: String = ""

    dynamic var transactions = List<TransactionRealm>()

    override static func primaryKey() -> String? { return "\(#keyPath(AccountRealm.id))" }

    convenience init(id: Int,
                     name: String) {
        self.init()
        self.id = id
        self.name = name
    }
}

extension AccountRealm {
    static func incrementalId(realm: Realm) -> Int {
        let primaryKey = "\(#keyPath(AccountRealm.id))"
        let biggestId = realm.objects(AccountRealm.self)
            .sorted(byKeyPath: primaryKey)
            .last?
            .id

        return (biggestId ?? 0) + 1
    }
}

extension AccountRealm {
    var toAccount: Account {
        return Account(id: id,
                       name: name)
    }

    var toAccountDetails: AccountDetails {
        let transactionsDomain: [Transaction] = transactions
            .compactMap({
                $0.transaction
            })
        return AccountDetails(id: id,
                              name: name,
                              transactions: transactionsDomain)
    }
}
