//
//  TransactionTypeRealm.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 27/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation
import TrackAppDomain

@objc
enum TransactionTypeRealm: Int {
    case income = 0
    case expense = 1
}

extension TransactionType {
    var toRealm: TransactionTypeRealm {
        switch self {
        case .income:
            return .expense
        case .expense:
            return .expense
        }
    }
}

extension TransactionTypeRealm {
    var transactionType: TransactionType {
        switch self {
        case .expense:
            return .expense
        case .income:
            return .income
        }
    }
}
