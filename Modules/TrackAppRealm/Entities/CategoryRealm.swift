//
//  CategoryRealm.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 27/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation
import RealmSwift
import TrackAppDomain
@objcMembers
class CategoryRealm: Object {
    dynamic var id: Int = 0
    dynamic var name: String = ""
    private dynamic var typeRawType = RealmOptional<Int>()

    var type: TransactionTypeRealm? {
        get {
            guard let rawInt = typeRawType.value else { return nil }
            return TransactionTypeRealm.init(rawValue: rawInt)
        } set {
            typeRawType.value = newValue?.rawValue
        }
    }


    override static func primaryKey() -> String? { return "\(#keyPath(CategoryRealm.id))" }

    convenience init(id: Int,
                     name: String,
                     type: TransactionTypeRealm) {
        self.init()
        self.id = id
        self.name = name
        self.type = type
    }
}

extension CategoryRealm {
    static func incrementalId(realm: Realm) -> Int {
        let primaryKey = "\(#keyPath(CategoryRealm.id))"
        let biggestId = realm.objects(CategoryRealm.self)
            .sorted(byKeyPath: primaryKey)
            .last?
            .id

        return (biggestId ?? 0) + 1
    }
}

extension CategoryRealm {
    var toCategory: TrackAppDomain.Category {
        TrackAppDomain.Category(id: id,
                                name: name,
                                transactionType: type?.transactionType ?? .income)
    }
}
