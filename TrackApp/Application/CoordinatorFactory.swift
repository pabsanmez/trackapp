//
//  CoordinatorFactory.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit
import TrackAppDomain

protocol CoordinatorFactoryType {
    func makeFinancialCoordinator(router: UINavigationController) -> FinancialCoordinator
}

struct CoordinatorFactory: CoordinatorFactoryType {

    private let moduleFactory: ModuleFactory
    private let serviceProvider: ServiceProvider

    init(moduleFactory: ModuleFactory,
         serviceProvider: ServiceProvider) {
        self.moduleFactory = moduleFactory
        self.serviceProvider = serviceProvider
    }

    func makeFinancialCoordinator(router: UINavigationController) -> FinancialCoordinator {
        FinancialCoordinator(router: router,
                             moduleFactory: moduleFactory,
                             services: serviceProvider.makeFinancialServices())
    }
}
