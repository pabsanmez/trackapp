//
//  ServiceProvider.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation
import RxSwift

public protocol ServiceProvider {
    func makeFinancialServices() -> FinancialServices
}
