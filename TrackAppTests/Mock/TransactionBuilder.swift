//
//  TransactionHelper.swift
//  TrackAppTests
//
//  Created by Pablo Sanchez Gomez on 29/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation
import TrackAppDomain
@testable import TrackApp

extension Transaction{
    static func build(transactionId: Int = .random(),
                      amount: Int = .random(),
                      timestamp: Double = .random(),
                      transactionType: TransactionType,
                      categoryId: Int = .random(),
                      categoryName: String = .random()) -> Transaction {
        let category = TrackAppDomain.Category(id: categoryId,
                                               name: categoryName,
                                               transactionType: transactionType)
        return Transaction(id: transactionId,
                           amount: amount,
                           timestamp: timestamp,
                           category: category)
    }
}

extension AccountDetails {
    static func build(id: Int = .random(),
                      name: String = .random(),
                      transactions: [Transaction]) -> AccountDetails {
        return AccountDetails(id: id,
                              name: name,
                              transactions: transactions)
    }
}
