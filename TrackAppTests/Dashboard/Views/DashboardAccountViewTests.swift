//
//  DashboardAccountViewTests.swift
//  TrackAppTests
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import XCTest
import SnapshotTesting
import TrackAppCommon
@testable import TrackApp

final class DashboardAccountViewTests: XCTestCase {

    override func setUp() {
        super.setUp()
        record = false
    }

    func test_GivenDashboardAcountView() {
        let viewmodel = DashboardAccountViewModel(title: "Title",
                                                  amount: "Amount")
        let sutView = DashboardAccountView()
        sutView.bind(viewModel: viewmodel)
        let contaner = sutView.embedInContainer()
        assertSnapshot(matching: contaner, as: .image)
    }
}

private extension UIView {
    func embedInContainer() -> UIView {
        let containerView = UIView(frame: CGRect(x: 0, y: 0, width: 375, height: 300))
        containerView.addSubview(self)
        self.makeLayout {
            $0.width == containerView.layout.width
            $0.centerSuperview()
        }
        return containerView
    }
}
