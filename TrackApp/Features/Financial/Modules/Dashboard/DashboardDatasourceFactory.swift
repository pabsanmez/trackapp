//
//  DashboardDatasourceFactory.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 29/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation
import TrackAppCommon
import TrackAppDomain

protocol DashboardDatasourceFactoryType {
    func mapToDashboardSectionType(accounts: [AccountDetails]) -> [DashboardViewSectionType]
}

struct DashboardDatasourceFactory: DashboardDatasourceFactoryType {

    private let currencyFormatter: CurrencyFormatterType
    private let timeZone: TimeZone

    init(currencyFormatter: CurrencyFormatterType = CurrencyFormatter.current,
         timeZone: TimeZone = TimeZone.current) {
        self.currencyFormatter =  currencyFormatter
        self.timeZone = timeZone
    }

    func mapToDashboardSectionType(accounts: [AccountDetails]) -> [DashboardViewSectionType] {
        return accounts.map({ account -> DashboardViewSectionType in
            let accountViewModel = DashboardAccountViewModel(title: account.name,
                                                             amount: currencyFormatter.format(amount: account.totalBalance))
            let transactions = account.transactions.map({
                DashboardTransactionCellViewModel(id: $0.id,
                                                  category: $0.category.name,
                                                  extraDetail: $0.timestamp.shortDatePresentation(timeZone: timeZone),
                                                  amount: currencyFormatter.format(amount: $0.amount),
                                                  isAnExpense: $0.type.isAnExpense)
            })
            return DashboardViewSectionType(account: accountViewModel,
                                            transactions: transactions)
        })
    }
}

extension AccountDetails {
    var totalBalance: Int {
        transactions.reduce(into: 0, { result, transaction in
            let transactionAmount = (transaction.type.isAnExpense ? -1 : 1) * transaction.amount
            result += transactionAmount
        })
    }
}

private extension Double {
    /// Returns date as dd/MM/yyyy
    func shortDatePresentation(timeZone: TimeZone) -> String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = timeZone
        dateFormatter.dateFormat = "dd/MM/yyyy, HH:mm"
        return dateFormatter.string(from: date)
    }
}

private extension TransactionType {
    var isAnExpense: Bool {
        switch self {
        case .income:
            return false
        case .expense:
            return true
        }
    }
}
