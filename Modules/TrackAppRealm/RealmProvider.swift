//
//  RealmProvider.swift
//  TrackAppRealm
//
//  Created by Pablo Sanchez Gomez on 04/05/2020.
//

import Foundation
import RealmSwift

struct RealmProvider {
    // The app requires Realm to work, so we are not going to create any kind of safe Init here. If the initialization of realm fails, the app will crash & we would get an error in our monitoring tool (as Crashlytics).
    static let shared = try! Realm()
}
