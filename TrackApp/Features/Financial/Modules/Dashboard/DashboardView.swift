import UIKit
import RxSwift

final class DashboardView: UIViewController {

    // MARK: - UI
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        return tableView
    }()

    // MARK: - Stored Properties
    private let viewModel: DashboardViewModelType
    private let disposeBag = DisposeBag()
    private var datasource: [DashboardViewSectionType] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    // MARK: - Subject
    private let addButtonTapSubject = PublishSubject<Void>()
    private let removedTransactionSubject = PublishSubject<Int>()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        bindViewModel()
    }
    
    init(viewModel: DashboardViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - UITableViewDataSource
extension DashboardView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource[section].transactions.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return datasource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DashboardTransactionCellView.identifier) as? DashboardTransactionCellView else { return UITableViewCell() }
        cell.selectionStyle = .none
        cell.bind(viewModel: datasource[indexPath.section].transactions[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionViewModel = datasource[section].account
        let view = DashboardAccountView()
        view.bind(viewModel: sectionViewModel)
        return view
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let idTransaction = datasource[indexPath.section].transactions[indexPath.row].id
        let action =  UIContextualAction(style: .destructive, title: Constants.delete, handler: { (action,view,completionHandler ) in
            self.removedTransactionSubject.onNext(idTransaction)
        })

        let configuration = UISwipeActionsConfiguration(actions: [action])
        return configuration
    }
}

// MARK: - Private methods
private extension DashboardView {
    func setupView() {
        registerCells()
        setupNavigationBar()

        view.addSubview(tableView)
        tableView.makeLayout {
            $0.fillSuperview(relativeToSafeArea: true)
        }
    }

    func setupNavigationBar() {
        navigationItem.title = Constants.title
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                            target: self,
                                                            action: #selector(addButtonTap))
    }
    
    func bindViewModel() {
        let input = DashboardViewModel.Input(viewIsLoaded: rx.viewIsLoaded,
                                             add: addButtonTapSubject,
                                             deleteTransacionTransactionId: removedTransactionSubject)

        let output = viewModel.transform(input: input)

        output.datasource
            .asDriverOnErrorJustComplete()
            .drive(onNext: { [weak self] in
                self?.datasource = $0
            })
            .disposed(by: disposeBag)

        output.actions
            .asDriverOnErrorJustComplete()
            .drive()
            .disposed(by: disposeBag)
    }

    func registerCells() {
        tableView.registerCell(cellIdentifier: DashboardTransactionCellView.identifier,
                               cellClass: DashboardTransactionCellView.self)
    }

    @objc
    func addButtonTap() {
        addButtonTapSubject.onNext(())
    }

    enum Constants {
        static var title: String { "Dashboard" }
        static var delete: String { "Delete" }
    }
}
