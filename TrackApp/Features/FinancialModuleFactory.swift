//
//  FinancialModuleFactory.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit
import TrackAppDomain

protocol FinancialModuleFactoryType {
    func makeDashboard(actions: DashboardViewModel.Actions,
                       services: FinancialServices) -> UIViewController

    func makeTransaction(actions: TransactionViewModel.Actions,
                         services: FinancialServices) -> UIViewController
}
