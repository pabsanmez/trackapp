//
//  DashboardTransactionCellViewTests.swift
//  TrackAppTests
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import XCTest
import SnapshotTesting
@testable import TrackApp

final class DashboardTransactionCellViewTests: XCTestCase {

    override func setUp() {
        super.setUp()
        record = false
    }

    func test_GivenDashboardTransactionCell_WhenAmountIsAnIncome() {
        let viewmodel = DashboardTransactionCellViewModel(id: 1,
                                                          category: "Category",
                                                          extraDetail: "Extra Detail",
                                                          amount: "13",
                                                          isAnExpense: true)
        let sutView = DashboardTransactionCellView()
        sutView.bind(viewModel: viewmodel)
        snapshotCell(sutView)
    }

    func test_GivenDashboardTransactionCell_WhenAmountIsAnExpense() {
        let viewmodel = DashboardTransactionCellViewModel(id: 1,
                                                          category: "Category",
                                                          extraDetail: "Extra Detail",
                                                          amount: "13",
                                                          isAnExpense: false)
        let sutView = DashboardTransactionCellView()
        sutView.bind(viewModel: viewmodel)
        snapshotCell(sutView)
    }
}

