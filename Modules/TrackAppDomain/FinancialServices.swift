//
//  FinancialRepository.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import RxSwift
import Foundation

public protocol FinancialServices {
    func fetchAccountsDetails() -> Single<[AccountDetails]>
    func deleteTransaction(id: Int) -> Single<Void>
    func getAccountList() -> Single<[Account]>
    func getCategoryList() -> Single<[Category]>
    func createTransaction(transactionBody: TransactionBody) -> Single<Void>
}

public struct TransactionBody {
    public let amount: Int
    public let accountId: Int
    public let categoryId: Int
    public let timestamp: Double

    public init(amount: Int,
                accountId: Int,
                categoryId: Int,
                timestamp: Double) {
        self.amount = amount
        self.accountId = accountId
        self.categoryId = categoryId
        self.timestamp = timestamp
    }
}
