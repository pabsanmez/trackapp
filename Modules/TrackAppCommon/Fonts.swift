//
//  Fonts.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit

public enum Fonts {

    public enum Size: CGFloat {
        case body = 16
        case caption = 13
    }

    public static func system(ofSize size: Size, weight: UIFont.Weight) -> UIFont {
        return UIFont.systemFont(ofSize: size.rawValue, weight: weight)
    }
}
