//
//  String+SafeQueries.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 27/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation
import RealmSwift

extension String {
    func realmEqualTo(_ item: String) -> NSPredicate {
        return NSPredicate(format: self + " = %@", item)
    }
}
