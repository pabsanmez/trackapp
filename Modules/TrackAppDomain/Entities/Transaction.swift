//
//  Transaction.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 27/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation

public struct Transaction: Equatable {
    public let id: Int
    public let amount: Int
    public let timestamp: Double
    public let category: Category

    public init(id: Int,
                amount: Int,
                timestamp: Double,
                category: Category) {
        self.id = id
        self.amount = amount
        self.timestamp = timestamp
        self.category = category
    }
}

public extension Transaction {
    var type: TransactionType {
        return category.transactionType
    }
}
