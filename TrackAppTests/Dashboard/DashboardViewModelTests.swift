//
//  DashboardViewModelTests.swift
//  TrackAppTests
//
//  Created by Pablo Sanchez Gomez on 29/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import XCTest
import RxSwift
import TrackAppCommon
import TrackAppDomain
@testable import TrackApp

final class DashboardViewModelTests: XCTestCase {

    private var disposeBag: DisposeBag!
    private var financialServices: FinancialServicesMock!
    private var factoryDatasource: DashboardDatasourceFactoryMock!

    // MARK: - Subjects
    var viewIsLoadedSubject: PublishSubject<Void>!
    var addSubject: PublishSubject<Void>!
    var deleteTransacionTransactionIdSubject: PublishSubject<Int>!

    override func setUp() {
        super.setUp()
        initializeVariables()
    }

    func test_GivenDashboardViewModel_WhenViewIsLoaded_ThenFetchesAccounts() {
        let sutViewModel = makeDashboardViewModel()
        let sutOutput = sutViewModel.transform(input: mockInput)

        sutOutput.datasource
            .subscribe()
            .disposed(by: disposeBag)

        viewIsLoadedSubject.onNext(())

        XCTAssertEqual(financialServices.fetchAccountDetailsParameters.timesCalled, 1)
    }

    func test_GivenDashboardViewModel_WhenFetchedAccounts_ThenExpectedDatasource() {

        let datasourceExpectation = expectation(description: "datasourceExpectation")

        // Mock Factory DataSource Response
        let expectedAccount = DashboardAccountViewModel(title: "Title", amount: "Amount")
        let expectedTransaction = DashboardTransactionCellViewModel(id: 3,
                                                            category: "Category",
                                                            extraDetail: "ExtraDetail",
                                                            amount: "Amount",
                                                            isAnExpense: true)

        let expectedDashboardSectionResponse = [DashboardViewSectionType(account: expectedAccount,
                                                                         transactions: [expectedTransaction])]
        //

        factoryDatasource.mapToDashboardSectionTypeResponse = expectedDashboardSectionResponse

        // Mock FinancialServices Response.
        let category1Transaction = TransactionType.income
        let transaction1Amount = 300
        let transaction1Timestamp: Double = 200

        let transaction1 = Transaction.build(amount: transaction1Amount,
                                             timestamp: transaction1Timestamp,
                                             transactionType: category1Transaction)

        let category2Transaction = TransactionType.expense
        let transaction2Amount = 500
        let transaction2Timestamp: Double = 200 
        let transaction2 = Transaction.build(amount: transaction2Amount,
                                             timestamp: transaction2Timestamp,
                                             transactionType: category2Transaction)


        let accountId = 1
        let accountName = "accountName"
        let fetchAccountDetailsResponse = [AccountDetails(id: accountId,
                                                         name: accountName,
                                                         transactions: [transaction1, transaction2])]
        //

        financialServices.fetchAccountDetailsParameters.response = .just(fetchAccountDetailsResponse)

        let sutViewModel = makeDashboardViewModel()
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe
        sutOutput.datasource
            .subscribe(onNext: {
                XCTAssertEqual($0, expectedDashboardSectionResponse)
                datasourceExpectation.fulfill()
            })
            .disposed(by: disposeBag)

        // Emits.

        viewIsLoadedSubject.onNext(())

        waitForExpectations(timeout: 0.05, handler: nil)
        XCTAssertEqual(factoryDatasource.mapToDashboardSectionTypeInputAccounts, fetchAccountDetailsResponse)
    }

    func test_GivenDashboardViewModel_WhenTransactionIsDeleted_ThenDeleteTransactionAndFetchesData() {
        let expectedId = 202
        financialServices.deleteTransactionParameters.response = .just(())

        let sutViewModel = makeDashboardViewModel()
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe
        sutOutput.datasource.subscribe().disposed(by: disposeBag)
        sutOutput.actions.subscribe().disposed(by: disposeBag)

        // Subject Emits
        deleteTransacionTransactionIdSubject.onNext(expectedId)

        XCTAssertEqual(financialServices.deleteTransactionParameters.id, expectedId)
        XCTAssertEqual(financialServices.deleteTransactionParameters.timesCalled, 1)
        XCTAssertEqual(financialServices.fetchAccountDetailsParameters.timesCalled, 1)
    }

    func test_GivenDashboardViewModel_WhenAddIsTapped_ThenAddActionIsCalled() {

        var addTapTimesCalled = 0
        let mockAddTap: InputClosure<VoidClosure> = { _ in
            addTapTimesCalled += 1
        }

        let sutViewModel = makeDashboardViewModel(addTap: mockAddTap)
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe
        sutOutput.actions.subscribe().disposed(by: disposeBag)

        // Subject Emits
        addSubject.onNext(())

        XCTAssertEqual(addTapTimesCalled, 1)
    }

    func test_GivenDashboardViewModel_WhenTransactionIsCreated_ThenAddFetchDatasource() {

        var addTapTimesCalled = 0
        let mockAddTap: InputClosure<VoidClosure> = { transactionIsCreated in
            addTapTimesCalled += 1
            transactionIsCreated()
        }

        let sutViewModel = makeDashboardViewModel(addTap: mockAddTap)
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe
        sutOutput.actions.subscribe().disposed(by: disposeBag)
        sutOutput.datasource.subscribe().disposed(by: disposeBag)

        // Subject Emits
        addSubject.onNext(())

        XCTAssertEqual(addTapTimesCalled, 1)
        XCTAssertEqual(financialServices.fetchAccountDetailsParameters.timesCalled, 1)
    }
}

// MARK: - Utils.
private extension DashboardViewModelTests {
    func initializeVariables() {
        self.disposeBag = DisposeBag()
        self.financialServices = FinancialServicesMock()
        self.factoryDatasource = DashboardDatasourceFactoryMock()
        self.viewIsLoadedSubject = PublishSubject<Void>()
        self.addSubject = PublishSubject<Void>()
        self.deleteTransacionTransactionIdSubject = PublishSubject<Int>()
    }

    func makeDashboardViewModel(addTap: @escaping InputClosure<VoidClosure> = { _ in }) -> DashboardViewModelType {
        return DashboardViewModel(actions: .init(onAddTap: addTap),
                                  services: financialServices,
                                  dashboardFactory: factoryDatasource)
    }

    var mockInput: DashboardViewModel.Input {
        return DashboardViewModel.Input(viewIsLoaded: viewIsLoadedSubject,
                                        add: addSubject,
                                        deleteTransacionTransactionId: deleteTransacionTransactionIdSubject)
    }
}

private class DashboardDatasourceFactoryMock: DashboardDatasourceFactoryType {

    var mapToDashboardSectionTypeInputAccounts: [AccountDetails]?
    var mapToDashboardSectionTypeResponse: [DashboardViewSectionType]?
    func mapToDashboardSectionType(accounts: [AccountDetails]) -> [DashboardViewSectionType] {
        mapToDashboardSectionTypeInputAccounts = accounts
        guard let mapToDashboardSectionTypeResponse = mapToDashboardSectionTypeResponse
            else {
                XCTFail()
                return []
        }
        return mapToDashboardSectionTypeResponse
    }
}
