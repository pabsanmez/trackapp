//
//  RandomGenerator.swift
//  TrackAppTests
//
//  Created by Pablo Sanchez Gomez on 29/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation

extension String {
    static func random(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }

    static func random(maxLength: Int = Int.random(in: 1...25)) -> String {
        return random(length: Int.random(in: 1...maxLength))
    }
}

extension Int {
    static func random() -> Int {
        return Int.random(in: 0...1000000)
    }
}

extension Double {
    static func random() -> Double {
        return Double.random(in: 0...1000000)
    }
}

