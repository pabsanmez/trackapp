//
//  Realm+Extensions.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 27/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation
import RealmSwift

extension Realm {
    func safeWrite(writeClosure: () -> ()) {
        do {
            try write {
                writeClosure()
            }
        } catch {
            // LOG ERROR
        }
    }
}
