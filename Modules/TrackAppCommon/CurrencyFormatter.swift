//
//  CurrencyFormatter.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 29/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation

public protocol CurrencyFormatterType {
    func format(amount: Int) -> String
}

public struct CurrencyFormatter: CurrencyFormatterType {

    public static let current = CurrencyFormatter()

    private let locale: Locale
    
    init(locale: Locale = Locale.current) {
        self.locale = locale
    }

    public func format(amount: Int) -> String {
        let formatter = NumberFormatter()
        formatter.locale = locale
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = .currency

        formatter.maximumFractionDigits = 2

        let amountInEuros = transformCentsToEuros(cents: amount)

        return formatter.string(from: NSNumber(value: amountInEuros)) ?? ""
    }

    func transformCentsToEuros(cents: Int) -> Double {
        return Double(cents) / 100
    }
}
