import RxSwift
import Foundation
import TrackAppDomain
import TrackAppCommon

protocol DashboardViewModelType {
    func transform(input: DashboardViewModel.Input) -> DashboardViewModel.Output
}

struct DashboardViewSectionType: Equatable {
    let account: DashboardAccountViewModel
    let transactions: [DashboardTransactionCellViewModel]
}

struct DashboardViewModel: DashboardViewModelType {

    struct Input {
        let viewIsLoaded: Observable<Void>
        let add: Observable<Void>
        let deleteTransacionTransactionId: Observable<Int>
    }

    struct Output {
        let datasource: Observable<[DashboardViewSectionType]>
        let actions: Observable<Void>
    }

    struct Actions {
        let onAddTap: InputClosure<VoidClosure>
    }

    // MARK: - Private Stored Properties
    private let actions: Actions
    private let services: FinancialServices

    // MARK: - Subjects
    private let orderRemovedSubject = PublishSubject<Void>()
    private let onCreatedTransactionSubject = PublishSubject<Void>()
    private let dashboardFactory: DashboardDatasourceFactoryType
    
    // MARK: - Lifecycle
    init(actions: Actions,
         services: FinancialServices,
         dashboardFactory: DashboardDatasourceFactoryType) {
        self.services = services
        self.actions = actions
        self.dashboardFactory = dashboardFactory
    }

    // MARK: - DashboardViewModelType Methods.
    func transform(input: Input) -> Output {

        let datasource = Observable.merge(orderRemovedSubject,
                                          input.viewIsLoaded,
                                          onCreatedTransactionSubject)
            .flatMap ({
                self.services.fetchAccountsDetails()
            })
            .map(dashboardFactory.mapToDashboardSectionType(accounts:))

        let deleteTransaction = input.deleteTransacionTransactionId
            .flatMap(self.services.deleteTransaction(id:))
            .do(onNext: {
                self.orderRemovedSubject.onNext(())
            })

        let add = input.add
            .do(onNext: {
                self.actions.onAddTap({
                    self.onCreatedTransactionSubject.onNext(())
                })
            })

        let actions = Observable.merge(deleteTransaction.mapToVoid(),
                                       add)

        return Output(datasource: datasource,
                      actions: actions)
    }
}
