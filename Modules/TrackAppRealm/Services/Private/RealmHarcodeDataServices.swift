//
//  RealmPrepopulateServices.swift
//  TrackAppRealm
//
//  Created by Pablo Sanchez Gomez on 04/05/2020.
//

import Foundation
import RealmSwift

struct RealmHarcodeDataServices {

    private let realm: Realm

    init(realm: Realm = RealmProvider.shared) {
        self.realm = realm
    }

    func populateEmptyDatabase() {
        realm.safeWrite {
            ["Tax", "Grocery", "Entertraiment", "Gym", "Health"]
                .forEach {
                    let category = CategoryRealm(id: CategoryRealm.incrementalId(realm: realm),
                                                 name: $0,
                                                 type: .expense)
                    realm.create(CategoryRealm.self, value: category, update: .all)
            }

            ["Salary", "Dividends"]
                .forEach {
                    let category = CategoryRealm(id: CategoryRealm.incrementalId(realm: realm),
                                                 name: $0,
                                                 type: .income)
                    realm.create(CategoryRealm.self, value: category, update: .all)
            }

            ["Cash", "Credit Card", "Bank account"]
                .forEach({
                    let account = AccountRealm(id: AccountRealm.incrementalId(realm: realm),
                                               name: $0)
                    realm.create(AccountRealm.self, value: account)
                })

            createTransaction(amount: 50,
                              accountId: 2,
                              categoryId: 3,
                              timestamp: Date().timeIntervalSince1970)

            createTransaction(amount: 200,
                              accountId: 1,
                              categoryId: 6,
                              timestamp: Date().timeIntervalSince1970)

            createTransaction(amount: 100,
                              accountId: 2,
                              categoryId: 3,
                              timestamp: Date().timeIntervalSince1970)

            createTransaction(amount: 100,
                              accountId: 3,
                              categoryId: 1,
                              timestamp: Date().timeIntervalSince1970)
        }

        func createTransaction(amount: Int,
                               accountId: Int,
                               categoryId: Int,
                               timestamp: Double) {
            guard let transaction = TransactionRealm.build(realm: realm,
                                                           categoryId: categoryId,
                                                           amount: amount,
                                                           timestamp: timestamp),
                let accountRealm = realm.object(ofType: AccountRealm.self,
                                                forPrimaryKey: accountId)
                else {
                    // Return Error
                    return
            }
            accountRealm.transactions.append(transaction)
        }
    }
}
