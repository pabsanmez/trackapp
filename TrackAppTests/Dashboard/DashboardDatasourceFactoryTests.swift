//
//  DashboardDatasourceFactoryTests.swift
//  TrackAppTests
//
//  Created by Pablo Sanchez Gomez on 29/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import XCTest
import TrackAppCommon
import TrackAppDomain
@testable import TrackApp

final class DashboardDatasourceFactoryTests: XCTestCase {

    private var currencyFormatter: CurrencyFormatterMock!
    private var sutDatasourceFactory: DashboardDatasourceFactory!

    override func setUp() {
        super.setUp()
        currencyFormatter = CurrencyFormatterMock()
        sutDatasourceFactory = DashboardDatasourceFactory(currencyFormatter: currencyFormatter,
                                                          timeZone: TimeZone(secondsFromGMT: 0)!)
    }

    func test_GivenDataSourceFactory_GivenExpenseTransactionType_ThenIsAnExpense() {
        let transaction = Transaction.build(transactionType: .expense)
        let mockAccount = AccountDetails.build(transactions: [transaction])

        let sutResult = sutDatasourceFactory.mapToDashboardSectionType(accounts: [mockAccount])
        XCTAssertEqual(sutResult.first?.transactions.first?.isAnExpense, true)
    }

    func test_GivenDataSourceFactory_GivenIncomeTransactionType_ThenIsNotAnExpense() {
        let transaction = Transaction.build(transactionType: .income)
        let mockAccount = AccountDetails.build(transactions: [transaction])

        let sutResult = sutDatasourceFactory.mapToDashboardSectionType(accounts: [mockAccount])
        XCTAssertEqual(sutResult.first?.transactions.first?.isAnExpense, false)
    }

    func test_GivenDataSourceFactory_GivenTransactionDate_ThenExpectedFormat() {
        // 1590710400 == 29/05/2020 @ 12:00am (UTC)
        let transaction = Transaction.build(timestamp: 1590710400,
                                            transactionType: .income)

        let mockAccount = AccountDetails.build(transactions: [transaction])

        let sutResult = sutDatasourceFactory.mapToDashboardSectionType(accounts: [mockAccount])
        XCTAssertEqual(sutResult.first?.transactions.first?.extraDetail, "29/05/2020, 00:00")
    }

    func test_GivenDataSourceFactory_GivenAccount_ThenExpectedDatasource() {

        // Mock Data.
        let transaction1 = Transaction.build(amount: 300,
                                             timestamp: 200, 
                                             transactionType: .income)

        let transaction2 = Transaction.build(amount: 500,
                                             timestamp: 200,
                                             transactionType: .expense)

        let accountId = 1
        let accountName = "accountName"
        let mockAccount = AccountDetails(id: accountId,
                                         name: accountName,
                                         transactions: [transaction1, transaction2])

        //
        let sutDatasourceFactory = DashboardDatasourceFactory(currencyFormatter: currencyFormatter)
        let datasource = sutDatasourceFactory.mapToDashboardSectionType(accounts: [mockAccount])

        XCTAssertEqual(datasource.count, 1)
        guard let firstSection = datasource.first else {
            XCTFail()
            return
        }

        XCTAssertEqual(firstSection.account.title, accountName)
        let expectedFirstSectionAmount = currencyFormatter.format(amount: transaction1.amount - transaction2.amount)
        XCTAssertEqual(firstSection.account.amount, expectedFirstSectionAmount)

        XCTAssertEqual(firstSection.transactions.count, 2)
        guard let firstTransaction = firstSection.transactions.first else {
            XCTFail()
            return
        }

        XCTAssertEqual(firstTransaction.amount, currencyFormatter.format(amount: transaction1.amount))
        XCTAssertEqual(firstTransaction.category, transaction1.category.name)
        XCTAssertEqual(firstTransaction.isAnExpense, false)
        XCTAssertEqual(firstTransaction.id, transaction1.id)

    }
}

private class CurrencyFormatterMock: CurrencyFormatterType {
    func format(amount: Int) -> String {
        return String(amount)
    }
}
