//
//  ModuleFactory.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit
import TrackAppDomain
struct ModuleFactory: FinancialModuleFactoryType {
    func makeDashboard(actions: DashboardViewModel.Actions,
                       services: FinancialServices) -> UIViewController {
        return DashboardView(viewModel: DashboardViewModel(actions: actions,
                                                           services: services,
                                                           dashboardFactory: DashboardDatasourceFactory()))
    }

    func makeTransaction(actions: TransactionViewModel.Actions,
                         services: FinancialServices) -> UIViewController {
        return TransactionView(viewModel: TransactionViewModel(actions: actions,
                                                               services: services))
    }
}
