//
//  TransactionViewTests.swift
//  TrackAppTests
//
//  Created by Pablo Sanchez Gomez on 29/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import XCTest
import RxSwift
import SnapshotTesting
@testable import TrackApp

final class TransactionViewTests: XCTestCase {

    override func setUp() {
        super.setUp()
        record = false
    }

    func test_GivenTransactionView() {
        let viewModel = MockTransactionViewModel()
        viewModel.accountTitle = .just("Account Title")
        viewModel.categoryTitle = .just("Category Title")
        viewModel.isDoneEnabledTitle = .just(true)


        let sutView = TransactionView(viewModel: viewModel)
        assertSnapshot(matching: sutView, as: .image())
    }
}

private class MockTransactionViewModel: TransactionViewModelType {

    var categoryTitle: Observable<String>?
    var accountTitle: Observable<String>?
    var isDoneEnabledTitle: Observable<Bool>?

    func transform(input: TransactionViewModel.Input) -> TransactionViewModel.Output {
        return .init(actions: .never(),
                     categoryTitle: categoryTitle ?? .never(),
                     accountTitle: accountTitle ?? .never(),
                     showPickerAccounts: .never(),
                     showPickerCategory: .never(),
                     isDoneEnabled: isDoneEnabledTitle ?? .never())
    }
}



