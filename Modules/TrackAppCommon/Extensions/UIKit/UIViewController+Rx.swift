//
//  UIViewController+.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import RxSwift
import RxCocoa

public extension Reactive where Base: UIViewController {

    var viewIsLoaded: Observable<Void> {
        return base.rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .take(1)
    }
}
