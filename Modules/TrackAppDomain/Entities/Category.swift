//
//  Category.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 27/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation

public struct Category: Equatable {
    public let id: Int
    public let name: String
    public let transactionType: TransactionType

    public init(id: Int,
                name: String,
                transactionType: TransactionType) {
        self.id = id
        self.name = name
        self.transactionType = transactionType

    }
}

public enum TransactionType {
    case income
    case expense
}
