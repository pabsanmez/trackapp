Pod::Spec.new do |s|
    s.name     = 'TrackAppRealm'
    s.version  = '1.0.0'
    s.summary  = "Utils"
    s.author   = { "TrackAppRealm" => "pabsanmez@gmail.com" }
    s.ios.deployment_target = '12.0'
    s.homepage = "https://github.com/pabsanmez"
    s.source   = { :git => "", :tag => "#{s.version}" }
    s.source_files = '**/*.swift'
    s.exclude_files = 'UnitTests/**/*'

    s.dependency 'RealmSwift'
    s.dependency 'TrackAppDomain'

    s.test_spec 'UnitTests' do |test_spec|
        test_spec.source_files = 'UnitTests/**/*.swift'
        test_spec.ios.resources = 'UnitTests/**/*.{json}'
    end
end


