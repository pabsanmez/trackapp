//
//  AppDelegate.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit
import RealmSwift
import TrackAppDomain
import TrackAppRealm

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    lazy var appCoordinator: AppCoordinator = {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = .white
        window.makeKeyAndVisible()

        let coordinatorFactory = CoordinatorFactory(moduleFactory: ModuleFactory(),
                                                    serviceProvider: RealmServiceProvider.shared)
        let appCoordinator = AppCoordinator(window: window, coordinatorFactory: coordinatorFactory)
        return appCoordinator
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        appCoordinator.start()

        return true
    }
}
