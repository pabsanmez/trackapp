import UIKit
import RxSwift
import TrackAppCommon

final class TransactionView: UIViewController {

    enum TransactionType {
        case income
        case expense

        fileprivate var name: String {
            switch self {
            case .expense:
                return Constants.expenseTitle
            case .income:
                return Constants.incomeTitle
            }
        }
    }

    // MARK: - UI
    private let categoryButton = UIButton(type: .system)
    private let accountButton = UIButton(type: .system)
    private let amountTextField = UITextField()
    private let transactionTypeSegmentedControl = UISegmentedControl()

    // MARK: - Stored Properties
    private let viewModel: TransactionViewModelType
    private let segmentedControlDatasource: [TransactionType] = {
        return [.income, .expense]
    }()

    private let disposeBag = DisposeBag()

    // MARK: - Subjects
    private let cancelButtonTapSubject = PublishSubject<Void>()
    private let doneButtonTapSubject = PublishSubject<Void>()
    private let categoryIdSelectedSubject = PublishSubject<String>()
    private let accountIdSelectedSubject = PublishSubject<String>()

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        bindViewModel()
    }
    
    init(viewModel: TransactionViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Private methods
private extension TransactionView {
    func setupView() {
        setupNavigationBar()
        setupTransaction()
        view.backgroundColor = Palette.background.primary
        amountTextField.backgroundColor = Palette.background.secondary
        amountTextField.placeholder = Constants.amountPlaceholderTitle
        amountTextField.textAlignment = .center

        let contentStackView = UIStackView(views: [transactionTypeSegmentedControl,
                                                   categoryButton,
                                                   accountButton,
                                                   amountTextField,
                                                   UIView()],
                                           axis: .vertical,
                                           spacing: 8)

        view.addSubview(contentStackView)
        contentStackView.makeLayout {
            $0.fillSuperview(with: UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16),
                             relativeToSafeArea: true)
        }
    }

    func setupNavigationBar() {
        navigationItem.title = Constants.title
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .cancel,
                                                                target: self,
                                                                action: #selector(cancelTap))

        navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done,
                                                                 target: self,
                                                                 action: #selector(doneTap))
    }

    func setupTransaction() {
        transactionTypeSegmentedControl.removeAllSegments()
        segmentedControlDatasource
            .reversed()
            .forEach({
                transactionTypeSegmentedControl.insertSegment(withTitle: $0.name, at: 0, animated: false)
        })
        transactionTypeSegmentedControl.selectedSegmentIndex = 0
    }
    
    func bindViewModel() {
        let amountInput = amountTextField.rx.text
            .map({ $0.orEmpty })

        let transactionTypeInput = transactionTypeSegmentedControl.rx.selectedSegmentIndex
            .compactMap({ [weak self] in
                return self?.segmentedControlDatasource[$0]
            })
        
        let input = TransactionViewModel.Input(viewIsLoaded: rx.viewIsLoaded,
                                               done: doneButtonTapSubject,
                                               cancel: cancelButtonTapSubject,
                                               accountButtonTap: accountButton.rx.tap.mapToVoid(),
                                               selectedAccountId: accountIdSelectedSubject,
                                               categoryButtonTap: categoryButton.rx.tap.mapToVoid(),
                                               selectedCategoryId: categoryIdSelectedSubject,
                                               amount: amountInput,
                                               transactionType: transactionTypeInput)
        let output = viewModel.transform(input: input)

        output.actions
            .asDriverOnErrorJustComplete()
            .drive()
            .disposed(by: disposeBag)

        output.showPickerAccounts
            .asDriverOnErrorJustComplete()
            .drive(onNext: { [weak self] in
                self?.presentPickerView(viewModel: $0,
                                        onSelectedRow: {
                                            self?.accountIdSelectedSubject.onNext($0) })
            })
            .disposed(by: disposeBag)

        output.showPickerCategory
            .asDriverOnErrorJustComplete()
            .drive(onNext: { [weak self] in
                self?.presentPickerView(viewModel: $0,
                                        onSelectedRow: {
                                            self?.categoryIdSelectedSubject.onNext($0) })
            })
            .disposed(by: disposeBag)

        output.accountTitle
        .asDriverOnErrorJustComplete()
            .drive(onNext: { [weak self] in
                self?.accountButton.setTitle($0, for: .normal)
            })
        .disposed(by: disposeBag)

        output.categoryTitle
        .asDriverOnErrorJustComplete()
            .drive(onNext: { [weak self] in
                self?.categoryButton.setTitle($0, for: .normal)
            })
        .disposed(by: disposeBag)

        output.isDoneEnabled
            .asDriverOnErrorJustComplete()
            .drive(onNext: { [weak self] in
                self?.navigationItem.rightBarButtonItem?.isEnabled = $0
            })
            .disposed(by: disposeBag)
    }

    @objc func cancelTap() {
        cancelButtonTapSubject.onNext(())
    }

    @objc func doneTap() {
        doneButtonTapSubject.onNext(())
    }

    enum Constants {
        static var title: String { "Add transaction" }
        static var expenseTitle: String { "Expense" }
        static var incomeTitle: String { "Income" }
        static var amountPlaceholderTitle: String { "Enter Amount in cents (no decimals)" }
    }
}
