//
//  DashboardViewTests.swift
//  TrackAppTests
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import XCTest
import RxSwift
import SnapshotTesting
@testable import TrackApp

final class DashboardViewTests: XCTestCase {

    override func setUp() {
        super.setUp()
        record = false
    }

    func test_GivenDashboardView() {
        let viewModel = MockDashboardViewModel()
        let expectedAccount = DashboardAccountViewModel(title: "Title", amount: "100")
        let expectedTransactions: [DashboardTransactionCellViewModel] = [
            .init(id: 1,
                  category: "category1",
                  extraDetail: "extraDetail1",
                  amount: "100",
                  isAnExpense: true),
            .init(id: 2,
                  category: "category2",
                  extraDetail: "extraDetail2",
                  amount: "300",
                  isAnExpense: false)
        ]

        viewModel.datasource = .just([
            .init(account: expectedAccount, transactions: expectedTransactions),
            .init(account: expectedAccount, transactions: expectedTransactions)
        ])

        let sutView = DashboardView(viewModel: viewModel)
        assertSnapshot(matching: sutView, as: .image())
    }
}

private class MockDashboardViewModel: DashboardViewModelType {

    var datasource: Observable<[DashboardViewSectionType]>?
    func transform(input: DashboardViewModel.Input) -> DashboardViewModel.Output {
        return DashboardViewModel.Output(datasource: datasource ?? .never(), actions: .never())
    }
}


