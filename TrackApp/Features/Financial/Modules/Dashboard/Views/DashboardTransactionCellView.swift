//
//  DashboardTransactionCellView.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit
import TrackAppCommon

struct DashboardTransactionCellViewModel: Equatable {
    let id: Int
    let category: String
    let extraDetail: String
    let amount: String
    let isAnExpense: Bool
}

final class DashboardTransactionCellView: UITableViewCell {

    private let categoryLabel: UILabel = .mainLabel
    private let extraDetailLabel: UILabel = .secondaryLabel
    private let amountLabel: UILabel = .amountLabel
    private let separatorView: UIView = HorizontalSeparatorView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func bind(viewModel: DashboardTransactionCellViewModel) {
        categoryLabel.text = viewModel.category
        extraDetailLabel.text = viewModel.extraDetail
        amountLabel.text = viewModel.amount
        amountLabel.textColor = viewModel.isAnExpense ? Palette.utility.expense : Palette.utility.income
    }
}

private extension DashboardTransactionCellView {
    func setupView() {
        let leftTextsStackView = UIStackView(views: [categoryLabel, extraDetailLabel],
                                             axis: .vertical,
                                             spacing: 4)
        let contentStackView = UIStackView(views: [leftTextsStackView, amountLabel],
                                           axis: .horizontal,
                                           spacing: 8)

        contentView.addSubview(contentStackView)
        contentView.addSubview(separatorView)

        contentStackView.makeLayout {
            $0.fillSuperview(with: UIEdgeInsets(top: 8,
                                                left: 16,
                                                bottom: 8,
                                                right: 16))
        }

        separatorView.makeLayout {
            $0.bottom == contentView.layout.bottom
            $0.left == contentView.layout.left
            $0.right == contentView.layout.right
        }
    }
}

// MARK: UI Factory
private extension UIView {

    static var amountLabel: UILabel {
        let label = UILabel()
        label.font = Fonts.system(ofSize: .body, weight: .regular)
        label.textAlignment = .right
        return label
    }

    static var mainLabel: UILabel {
        let label = UILabel()
        label.font = Fonts.system(ofSize: .body, weight: .regular)
        label.textColor = Palette.text.primary
        return label
    }

    static var secondaryLabel: UILabel {
        let label = UILabel()
        label.font = Fonts.system(ofSize: .caption, weight: .regular)
        label.textColor = Palette.text.secondary
        return label
    }
}
