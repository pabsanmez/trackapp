//
//  UITableViewCell+Extensions.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit

public extension UITableViewCell {
    static var identifier: String { return String(describing: self) }
    static var cellClass: AnyClass { return self }
}
