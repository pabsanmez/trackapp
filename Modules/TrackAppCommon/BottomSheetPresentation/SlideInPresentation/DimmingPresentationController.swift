//
//  DimmingPresentationController.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit

final class DimmingPresentationController: UIPresentationController {

    // MARK: - Properties
    private var dimmingView: UIView?

    override public init(presentedViewController: UIViewController,
                  presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController,
                   presenting: presentingViewController)

        setupDimmingView()
    }

    override public func presentationTransitionWillBegin() {
        guard let dimmingView = dimmingView else { return }
        containerView?.addSubview(dimmingView)
        dimmingView.makeLayout {
            $0.fillSuperview()
        }

        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 1.0
            return
        }

        coordinator.animate(alongsideTransition: { _ in
            self.dimmingView?.alpha = 1.0
        })
    }

    override public func dismissalTransitionWillBegin() {
        guard let coordinator = presentedViewController.transitionCoordinator else {
            UIView.animate(withDuration: 0.2) {
                self.dimmingView?.alpha = 0.0
            }
            return
        }

        coordinator.animate(alongsideTransition: { _ in
            UIView.animate(withDuration: 0.2) {
                self.dimmingView?.alpha = 0.0
            }
        })
    }
}

// MARK: - Private
private extension DimmingPresentationController {
    func setupDimmingView() {
        dimmingView = UIView()
        dimmingView?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        dimmingView?.alpha = 0.0
    }
}
