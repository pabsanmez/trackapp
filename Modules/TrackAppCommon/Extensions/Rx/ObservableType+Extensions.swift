//
//  ObservableType+Extensions.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import RxSwift
import RxCocoa

public extension ObservableType {
    func mapToVoid() -> Observable<Void> {
        return map { _ in }
    }

    func asDriverOnErrorJustComplete() -> Driver<Element> {
        return asDriver { error in
            // Track me.
            assertionFailure()
            return Driver.empty()
        }
    }
}
