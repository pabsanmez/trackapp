//
//  Nais.swift
//  Pods-TrackApp
//
//  Created by Pablo Sanchez Gomez on 03/05/2020.
//

import Foundation

public typealias VoidClosure = () -> Void
public typealias InputClosure<T> = (_: T) -> Void
