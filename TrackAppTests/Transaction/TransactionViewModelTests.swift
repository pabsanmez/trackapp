//
//  TransactionViewModelTests.swift
//  TrackAppTests
//
//  Created by Pablo Sanchez Gomez on 29/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import XCTest
import RxSwift
import TrackAppCommon
import TrackAppDomain
@testable import TrackApp

final class TransactionViewModelTests: XCTestCase {

    private var disposeBag: DisposeBag!
    private var financialServices: FinancialServicesMock!

    // MARK: - Subjects
    private var viewIsLoadedSubject: PublishSubject<Void>!
    private var doneSubject: PublishSubject<Void>!
    private var cancelSubject: PublishSubject<Void>!
    private var accountButtonTapSubject: PublishSubject<Void>!
    private var selectedAccountIdSubject: PublishSubject<String>!
    private var categoryButtonTapSubject: PublishSubject<Void>!
    private var selectedCategoryIdSubject: PublishSubject<String>!
    private var amountSubject: PublishSubject<String>!
    private var transactionTypeSubject: PublishSubject<TransactionView.TransactionType>!

    override func setUp() {
        super.setUp()
        initializeVariables()
    }

    func test_GivenTransactionViewModel_WhenViewDidLoad_ThenFetchCategoryListAndAccountList() {
        let sutViewModel = makeViewModel()
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe.
        sutOutput.actions.subscribe().disposed(by: disposeBag)

        // Emits.
        viewIsLoadedSubject.onNext(())

        XCTAssertEqual(financialServices.getAccountParameters.timesCalled, 1)
        XCTAssertEqual(financialServices.getCategoryListParameters.timesCalled, 1)
    }

    func test_GivenTransactionViewModel_WhenCancel_ThenCancelIsTriggered() {
        let cancelExpectation = expectation(description: "cancelExpectation")

        let cancelClosure: VoidClosure = {
            cancelExpectation.fulfill
        }()

        let sutViewModel = makeViewModel(cancelClosure: cancelClosure)
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe.
        sutOutput.actions.subscribe().disposed(by: disposeBag)

        // Emits.
        cancelSubject.onNext(())

        waitForExpectations(timeout: 0.05, handler: nil)
    }

    func test_GivenTransactionViewModel_WhenAccountIsSelected_ThenAccountTitleEmits() {
        let sutViewModel = makeViewModel()
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe.
        sutOutput.actions.subscribe().disposed(by: disposeBag)

        // Emits.
        viewIsLoadedSubject.onNext(())

        XCTAssertEqual(financialServices.getAccountParameters.timesCalled, 1)
        XCTAssertEqual(financialServices.getCategoryListParameters.timesCalled, 1)
    }

    func test_GivenTransactionViewModel_WhenCategoryButtonTapWithTransactionTypeIncome_ThenShowPickerCategoryEmitsWithSelectedIdAndFilteredIncomes() {
        let showPickerCategoryExpectation = expectation(description: "showPickerCategoryExpectation")
        let selectedCategoryId = 4
        let mockCategoryListResponse: [TrackAppDomain.Category] = [.init(id: 3,
                                                                         name: "IncomeType3",
                                                                         transactionType: .income),
                                                                   .init(id: selectedCategoryId,
                                                                         name: "IncomeType4",
                                                                         transactionType: .income),
                                                                   .init(id: 5,
                                                                         name: "ExpenseType5",
                                                                         transactionType: .expense)]
        financialServices.getCategoryListParameters.response = .just(mockCategoryListResponse)
        financialServices.getAccountParameters.response = .just([])

        let sutViewModel = makeViewModel()
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe.
        sutOutput.actions.subscribe().disposed(by: disposeBag)
        sutOutput.showPickerCategory
            .subscribe(onNext: { pickerCategoryData in
                XCTAssertEqual(pickerCategoryData.selectedId, String(selectedCategoryId))
                XCTAssertEqual(pickerCategoryData.datasource.count, 2)
                XCTAssertEqual(pickerCategoryData.datasource[0].id, String(mockCategoryListResponse[0].id))
                XCTAssertEqual(pickerCategoryData.datasource[1].id, String(mockCategoryListResponse[1].id))
                XCTAssertEqual(pickerCategoryData.datasource[0].name, mockCategoryListResponse[0].name)
                XCTAssertEqual(pickerCategoryData.datasource[1].name, mockCategoryListResponse[1].name)
                showPickerCategoryExpectation.fulfill()
            }).disposed(by: disposeBag)

        // Emits.
        viewIsLoadedSubject.onNext(())
        transactionTypeSubject.onNext(.income)
        selectedCategoryIdSubject.onNext(String(selectedCategoryId))
        categoryButtonTapSubject.onNext(())

        waitForExpectations(timeout: 0.05, handler: nil)
    }


    func test_GivenTransactionViewModel_WhenViewIsLoaded_ThenCategoryTitleEmitsEmptyCase() {
        let categoryTitleExpectation = expectation(description: "categoryTitleExpectation")

        let sutViewModel = makeViewModel()
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe.
        sutOutput.actions.subscribe().disposed(by: disposeBag)
        sutOutput.categoryTitle
            .subscribe(onNext: { categoryTitle in
                XCTAssertEqual(categoryTitle, "Select a category")
                categoryTitleExpectation.fulfill()
            }).disposed(by: disposeBag)

        // Emits.
        viewIsLoadedSubject.onNext(())

        waitForExpectations(timeout: 0.05, handler: nil)
    }

    func test_GivenTransactionViewModel_WhenViewIsLoaded_ThenCategoryTitleEmitsExpectedTitle() {
        let categoryTitleExpectation = expectation(description: "categoryTitleExpectation")
        let selectedCategoryId = 4
        let selectedCategoryName = "IncomeType4"
        let mockCategoryListResponse: [TrackAppDomain.Category] = [.init(id: 3,
                                                                         name: "IncomeType3",
                                                                         transactionType: .income),
                                                                   .init(id: selectedCategoryId,
                                                                         name: selectedCategoryName,
                                                                         transactionType: .income),
                                                                   .init(id: 5,
                                                                         name: "ExpenseType5",
                                                                         transactionType: .expense)]
        financialServices.getCategoryListParameters.response = .just(mockCategoryListResponse)
        financialServices.getAccountParameters.response = .just([])

        let sutViewModel = makeViewModel()
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe.
        sutOutput.actions.subscribe().disposed(by: disposeBag)
        sutOutput.categoryTitle
            .skip(1) // Skip initial output.
            .subscribe(onNext: { categoryTitle in
                XCTAssertEqual(categoryTitle, "Category Selected: \(selectedCategoryName)")
                categoryTitleExpectation.fulfill()
            }).disposed(by: disposeBag)

        // Emits.
        viewIsLoadedSubject.onNext(())
        transactionTypeSubject.onNext(.income)
        selectedCategoryIdSubject.onNext(String(selectedCategoryId))

        waitForExpectations(timeout: 0.05, handler: nil)
    }

    func test_GivenTransactionViewModel_WhenTransactionBodyIsNotFilled_ThenDoneIsNotEnabled() {
        let isDoneEnabledExpectation = expectation(description: "isDoneEnabledExpectation")

        let sutViewModel = makeViewModel()
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe.
        sutOutput.actions.subscribe().disposed(by: disposeBag)
        sutOutput.isDoneEnabled
            .subscribe(onNext: { isDoneButtonEnabled in
                XCTAssertFalse(isDoneButtonEnabled)
                isDoneEnabledExpectation.fulfill()
            }).disposed(by: disposeBag)

        // Emits.
        viewIsLoadedSubject.onNext(())

        waitForExpectations(timeout: 0.05, handler: nil)
    }

    func test_GivenTransactionViewModel_WhenTransactionBodyIsFilled_ThenDoneIsEnabled() {
        let isDoneEnabledExpectation = expectation(description: "isDoneEnabledExpectation")

        let selectedCategoryId = 4
        financialServices.getCategoryListParameters.response = .just([.init(id: selectedCategoryId,
                                                                            name: "IncomeType4",
                                                                            transactionType: .income)])
        let selectedAccountId = 3
        financialServices.getAccountParameters.response = .just([.init(id: selectedAccountId, name: "Account Mock")])

        let sutViewModel = makeViewModel()
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe.
        sutOutput.actions.subscribe().disposed(by: disposeBag)
        sutOutput.isDoneEnabled
            .skip(1) // Skip initial false statement
            .subscribe(onNext: { isDoneButtonEnabled in
                XCTAssertTrue(isDoneButtonEnabled)
                isDoneEnabledExpectation.fulfill()
            }).disposed(by: disposeBag)

        // Emits.
        viewIsLoadedSubject.onNext(())
        amountSubject.onNext("300")
        transactionTypeSubject.onNext(.income)
        selectedCategoryIdSubject.onNext(String(selectedCategoryId))
        selectedAccountIdSubject.onNext(String(selectedAccountId))

        waitForExpectations(timeout: 0.05, handler: nil)
    }

    func test_GivenTransactionViewModel_WhenDone_ThenCreateTransactionIsTriggered_DoneActionIsTriggered() {

        // Mock Responses:
        let selectedCategoryId = 4
        financialServices.getCategoryListParameters.response = .just([.init(id: selectedCategoryId,
                                                                            name: "IncomeType4",
                                                                            transactionType: .income)])
        let selectedAccountId = 3
        financialServices.getAccountParameters.response = .just([.init(id: selectedAccountId, name: "Account Mock")])

        financialServices.createTransactionParameters.response = .just(())
        //

        let doneExpectation = expectation(description: "doneExpectation")

        let expectedTimeStamp: Double = 37373633

        let doneClosure: VoidClosure = {
            doneExpectation.fulfill
        }()

        let sutViewModel = makeViewModel(currentDate: { Date(timeIntervalSince1970: expectedTimeStamp) } , doneClosure: doneClosure)
        let sutOutput = sutViewModel.transform(input: mockInput)

        // Subscribe.
        sutOutput.actions.subscribe().disposed(by: disposeBag)

        // Prepare Transaction Body.
        viewIsLoadedSubject.onNext(())
        amountSubject.onNext("300")
        transactionTypeSubject.onNext(.income)
        selectedCategoryIdSubject.onNext(String(selectedCategoryId))
        selectedAccountIdSubject.onNext(String(selectedAccountId))

        // Emits.
        doneSubject.onNext(())

        XCTAssertEqual(financialServices.createTransactionParameters.timesCalled, 1)
        XCTAssertEqual(financialServices.createTransactionParameters.transactionBody?.amount, 300)
        XCTAssertEqual(financialServices.createTransactionParameters.transactionBody?.accountId, selectedAccountId)
        XCTAssertEqual(financialServices.createTransactionParameters.transactionBody?.categoryId, selectedCategoryId)
        XCTAssertEqual(financialServices.createTransactionParameters.transactionBody?.timestamp, expectedTimeStamp)
        waitForExpectations(timeout: 0.05, handler: nil)
    }
}

// MARK: - Utils.
private extension TransactionViewModelTests {
    func initializeVariables() {
        self.disposeBag = DisposeBag()
        self.financialServices = FinancialServicesMock()
        self.viewIsLoadedSubject = PublishSubject<Void>()
        self.doneSubject = PublishSubject<Void>()
        self.cancelSubject = PublishSubject<Void>()
        self.accountButtonTapSubject = PublishSubject<Void>()
        self.selectedAccountIdSubject = PublishSubject<String>()
        self.categoryButtonTapSubject = PublishSubject<Void>()
        self.selectedCategoryIdSubject = PublishSubject<String>()
        self.amountSubject = PublishSubject<String>()
        self.transactionTypeSubject = PublishSubject<TransactionView.TransactionType>()
    }

    var mockInput: TransactionViewModel.Input {
        return TransactionViewModel.Input(viewIsLoaded: viewIsLoadedSubject,
                                          done: doneSubject,
                                          cancel: cancelSubject,
                                          accountButtonTap: accountButtonTapSubject,
                                          selectedAccountId: selectedAccountIdSubject,
                                          categoryButtonTap: categoryButtonTapSubject,
                                          selectedCategoryId: selectedCategoryIdSubject,
                                          amount: amountSubject,
                                          transactionType: transactionTypeSubject)
    }

    func makeViewModel(currentDate: @escaping () -> Date = { Date() },
                       doneClosure: @escaping VoidClosure = { },
                       cancelClosure: @escaping VoidClosure = { }) -> TransactionViewModel {
        let actions = TransactionViewModel.Actions(done: doneClosure,
                                                   cancel: cancelClosure)
        return TransactionViewModel(actions: actions,
                                    services: financialServices,
                                    currentDate: currentDate)
    }
}
