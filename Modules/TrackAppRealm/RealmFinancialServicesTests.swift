//
//  RealmFinancialServicesTests.swift
//  TrackAppRealm-Unit-UnitTests
//
//  Created by Pablo Sanchez Gomez on 03/05/2020.
//

import XCTest
import RxSwift
import RealmSwift
@testable import TrackAppRealm

class RealmFinancialServicesTests: XCTestCase {

    var sut: RealmFinancialServices!
    var realmMock: Realm!
    var disposeBag: DisposeBag!

    override func setUp() {
        super.setUp()
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
        realmMock = try! Realm()
        sut = RealmFinancialServices(realm: realmMock)
        disposeBag = DisposeBag()
    }

    override func tearDown() {
        try! realmMock.write {
            realmMock.deleteAll()
        }
        super.tearDown()
    }

    func test_GivenRealmFinancialServices_WhenFetchAccountDetails_ThenReturnStoredAccountDetails() {

        let mockAccounts: [AccountRealm] = [.init(id: 1, name: "name"),
                                            .init(id: 2, name: "name2")]

        try! realmMock.write {
            realmMock.add(mockAccounts)
        }

        let fetchAccountDetailsExpectation = expectation(description: "fetchAccountDetailsExpectation")
        sut.fetchAccountsDetails()
            .subscribe(onSuccess: { data in
                XCTAssertEqual(data.count, 2)
                XCTAssertEqual(data[0].id, 1)
                XCTAssertEqual(data[1].id, 2)
                fetchAccountDetailsExpectation.fulfill()
            })
            .disposed(by: disposeBag)

        waitForExpectations(timeout: 0.05, handler: nil)
    }

    func test_GivenRealmFinancialServices_WhenDeleteTransaction_ThenDeletesTransaction() {

        let mockAccounts: [TransactionRealm] = [
            .init(id: 3,
                  amount: 100,
                  timestamp: 100,
                  category: .init(id: 1,
                                  name: "name",
                                  type: .expense)),
            .init(id: 5,
                  amount: 200,
                  timestamp: 100,
                  category: .init(id: 2,
                                  name: "name",
                                  type: .expense))
        ]

        try! realmMock.write {
            realmMock.add(mockAccounts)
        }

        let deleteTransactionExpectations = expectation(description: "deleteTransactionExpectations")
        XCTAssertEqual(realmMock.objects(TransactionRealm.self).count, 2)

        sut.deleteTransaction(id: 3)
            .subscribe(onSuccess: { _ in
                XCTAssertEqual(self.realmMock.objects(TransactionRealm.self).count, 1)
                deleteTransactionExpectations.fulfill()
            })
            .disposed(by: disposeBag)

        waitForExpectations(timeout: 0.05, handler: nil)
    }

    func test_GivenRealmFinancialServices_WhenFetchAccountList_ThenReturnAccountList() {

        let mockAccounts: [AccountRealm] = [.init(id: 1, name: "name"),
                                            .init(id: 2, name: "name2")]

        try! realmMock.write {
            realmMock.add(mockAccounts)
        }

        let fetchAccountListExpectation = expectation(description: "fetchAccountListExpectation")
        sut.getAccountList()
            .subscribe(onSuccess: { data in
                XCTAssertEqual(data.count, 2)
                XCTAssertEqual(data[0].id, 1)
                XCTAssertEqual(data[1].id, 2)
                fetchAccountListExpectation.fulfill()
            })
            .disposed(by: disposeBag)

        waitForExpectations(timeout: 0.05, handler: nil)
    }

    func test_GivenRealmFinancialServices_WhenFetchCategoryList_ThenReturnCategoryList() {

        let mockCategories: [CategoryRealm] = [.init(id: 2, name: "name", type: .expense),
                                               .init(id: 4, name: "name", type: .expense)]

        try! realmMock.write {
            realmMock.add(mockCategories)
        }

        let fetchCategoryListExpectation = expectation(description: "fetchCategoryListExpectation")
        sut.getCategoryList()
            .subscribe(onSuccess: { data in
                XCTAssertEqual(data.count, 2)
                XCTAssertEqual(data[0].id, 2)
                XCTAssertEqual(data[1].id, 4)
                fetchCategoryListExpectation.fulfill()
            })
            .disposed(by: disposeBag)

        waitForExpectations(timeout: 0.05, handler: nil)
    }

    func test_GivenRealmFinancialServices_WhenCreateTransaction_ThenCreatesTransaction() {

        let mockAccountRealm = AccountRealm(id: 3, name: "2")
        let mockCategoryRealm = CategoryRealm(id: 2, name: "name", type: .expense)

        try! realmMock.write {
            realmMock.add(mockAccountRealm)
            realmMock.add(mockCategoryRealm)
        }

        XCTAssertEqual(realmMock.objects(TransactionRealm.self).count, 0)

        let createTransactionExpectation = expectation(description: "createTransactionExpectation")
        sut.createTransaction(transactionBody: .init(amount: 300,
                                                     accountId: 3,
                                                     categoryId: 2,
                                                     timestamp: 300))
            .subscribe(onSuccess: { _ in
                createTransactionExpectation.fulfill()
            })
            .disposed(by: disposeBag)

        waitForExpectations(timeout: 0.05, handler: nil)
        XCTAssertEqual(realmMock.objects(TransactionRealm.self).count, 1)
    }
}
