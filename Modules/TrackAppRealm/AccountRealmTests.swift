//
//  AccountRealmTests.swift
//  TrackAppRealm-Unit-UnitTests
//
//  Created by Pablo Sanchez Gomez on 03/05/2020.
//

import XCTest
import RealmSwift
@testable import TrackAppRealm

class AccountRealmTests: XCTestCase {

    var realmMock: Realm!

    override func setUp() {
        super.setUp()
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
        realmMock = try! Realm()
    }

    override func tearDown() {
        try! realmMock.write {
            realmMock.deleteAll()
        }
        super.tearDown()
    }

    func test_GivenAccount_WhenConvenienceInit_ThenExpectedResults() {
        let sutAccount = AccountRealm(id: 3, name: "accountName")

        XCTAssertEqual(sutAccount.id, 3)
        XCTAssertEqual(sutAccount.name, "accountName")
    }

    func test_GivenAccount_WhenToAccountDetails_ThenReturnsAccountDetails() {
        let transactionRealm = TransactionRealm(id: 27,
                                                amount: 100,
                                                timestamp: 10,
                                                category: .init(id: 3,
                                                                name: "CategoryName",
                                                                type: .income))
        let sutAccountRealm = AccountRealm(id: 3, name: "accountName")
        sutAccountRealm.transactions.append(transactionRealm)


        let account = sutAccountRealm.toAccountDetails
        XCTAssertEqual(account.id, 3)
        XCTAssertEqual(account.name, "accountName")
        XCTAssertEqual(account.transactions.count, 1)
        XCTAssertEqual(account.transactions.first?.id, 27)
    }

    func test_GivenAccount_WhenToAccount_ThenReturnsAccount() {
        let sutAccountRealm = AccountRealm(id: 3, name: "accountName")

        let account = sutAccountRealm.toAccount
        XCTAssertEqual(account.id, 3)
        XCTAssertEqual(account.name, "accountName")
    }

    func test_GivenAccount_WhenIncrementalId_ThenReturnsMaxId() {
        let biggestId = 3
        let sutAccount = AccountRealm(id: biggestId, name: "accountName")
        try! realmMock.write {
            realmMock.add(sutAccount)
        }

        XCTAssertEqual(AccountRealm.incrementalId(realm: realmMock), biggestId + 1)
    }
}
