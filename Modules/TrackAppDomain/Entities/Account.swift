//
//  Account.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 27/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation

public struct Account {
    public let id: Int
    public let name: String

    public init(id: Int,
                name: String) {
        self.id = id
        self.name = name
    }
}
