//
//  UITableView+Extensions.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit

public extension UITableView {
    func registerCell(cellIdentifier: String, cellClass: AnyClass) {
        let bundle = Bundle(for: cellClass)
        let resourceName = String(describing: cellClass)
        if bundle.path(forResource: resourceName, ofType: "nib") != nil {
            let nib = UINib(nibName: resourceName, bundle: bundle)
            register(nib, forCellReuseIdentifier: cellIdentifier)
        } else {
            register(cellClass, forCellReuseIdentifier: cellIdentifier)
        }
    }
}
