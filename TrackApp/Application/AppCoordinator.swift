//
//  AppCoordinator.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit

struct AppCoordinator {

    private let window: UIWindow
    private let coordinatorFactory: CoordinatorFactoryType

    init(window: UIWindow,
         coordinatorFactory: CoordinatorFactoryType) {
        self.window = window
        self.coordinatorFactory = coordinatorFactory
    }

    func start() {
        showFinancial()
    }
}

private extension AppCoordinator {
    func showFinancial() {
        let newRouter = UINavigationController()
        let financialCoord = coordinatorFactory.makeFinancialCoordinator(router: newRouter)
        financialCoord.start()
        window.rootViewController = newRouter
    }
}
